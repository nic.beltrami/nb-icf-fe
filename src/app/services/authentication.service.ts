import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class AuthenticationService {

    constructor() {
    }

    login(username: string, password: string) {
        const btoaValue = window.btoa(`${username}:${password}`);
        const authToken = `Basic ${btoaValue}`;
        localStorage.setItem('basicAuthTokenUser', JSON.stringify(authToken));
    }

    logout() {
        localStorage.removeItem('basicAuthTokenUser');
    }
}
