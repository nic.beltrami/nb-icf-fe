import { TestBed, getTestBed } from '@angular/core/testing';

import { DataService } from './data.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { DataValue } from '../model/data-value';
import { tap } from 'rxjs/operators';

describe('DataService tests', () => {

    let myProvider: DataService;
    let httpMock: HttpTestingController;
    let testBed;

    beforeEach(() => {

        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [DataService]
        });

        testBed = getTestBed();
        myProvider = testBed.get(DataService);
        httpMock = testBed.get(HttpTestingController);

    });

    it('should return an Observable<DataValue[]>', () => {

        const dataMock: DataValue[] = [];
        const d1 = new DataValue();
        const d2 = new DataValue();
        const d3 = new DataValue();
        dataMock.push(d1);
        dataMock.push(d2);
        dataMock.push(d3);

        myProvider.findAll().pipe(
            tap(data => {
                expect(data.length).toBe(3);
            })
        ).subscribe();
    });
});

