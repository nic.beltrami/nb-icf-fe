import { Injectable } from '@angular/core';
import { DataValue } from '../model/data-value';
import { HttpClient, HttpHeaders, HttpXsrfTokenExtractor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class DataService {

    private readonly basepath = '/data';

    constructor(private httpClient: HttpClient) {
    }

    public findAll(): Observable<DataValue[]> {
        return this.httpClient.get<DataValue[]>(`${environment.restUrl}${this.basepath}`, {headers: this.getHeaders(), withCredentials: true});
    }

    public descrypt(data: DataValue): Observable<any> {
        return this.httpClient.post(`${environment.restUrl}${this.basepath}/${data.id}/decrypt`, data, {headers: this.getHeaders(), withCredentials: true});
    }

    public insert(data: DataValue): Observable<any> {
        return this.httpClient.post(`${environment.restUrl}${this.basepath}`, data, {headers: this.getHeaders(), withCredentials: true});
    }

    public update(data: DataValue): Observable<any> {
        return this.httpClient.put(`${environment.restUrl}${this.basepath}`, data, {headers: this.getHeaders(), withCredentials: true});
    }

    public delete(id: number): Observable<any> {
        return this.httpClient.delete(`${environment.restUrl}${this.basepath}`, {headers: this.getHeaders(), withCredentials: true});
    }

    private getHeaders(): HttpHeaders {
        let authKey = localStorage.getItem('basicAuthTokenUser');
        authKey = authKey.replace(/"/g, '');
        return new HttpHeaders()
            .set('Access-Control-Allow-Methods', 'DELETE, POST, GET, OPTIONS')
            .set('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With')
            .set('Access-Control-Allow-Credentials', 'true')
            .set('Content-Type', 'application/json')
            .set('Authorization', authKey);
    }

}
