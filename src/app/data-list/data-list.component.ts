import { Component, OnDestroy, OnInit } from '@angular/core';
import { DataValue } from '../model/data-value';
import { DataService } from '../services/data.service';
import { AuthenticationService } from '../services/authentication.service';
import { tap } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';

@Component({
    selector: 'app-data-list',
    templateUrl: './data-list.component.html',
    styleUrls: ['./data-list.component.css']
})
export class DataListComponent implements OnInit, OnDestroy {

    dataSource: DataValue[] = [];
    headerColumns: string[] = [];
    selectedItem: DataValue;
    isEncrypted: boolean;

    constructor(private dataService: DataService, private authenticationService: AuthenticationService, private snackBar: MatSnackBar) {
    }

    ngOnInit() {
        this.isEncrypted = true;
        this.authenticationService.login('user', 'password');
        this.selectedItem = new DataValue();
        this.headerColumns = ['id', 'value'];
        this.getData();
    }

    selectItem(item: DataValue) {
        this.isEncrypted = true;
        this.selectedItem = {...item};
    }

    send() {
        if (this.selectedItem.id === undefined) {
            this.dataService.insert(this.selectedItem).pipe(
                tap(() => {
                    this.selectedItem = new DataValue();
                    this.getData();
                }, error => {
                    this.openSnackBar('Error', error.statusText);
                }),
            ).subscribe();
        } else {
            this.dataService.update(this.selectedItem).pipe(
                tap(() => {
                    this.selectedItem = new DataValue();
                    this.getData();
                }, error => {
                    this.openSnackBar('Error', error.statusText);
                }),
            ).subscribe();
        }
    }

    resetItem() {
        this.isEncrypted = false;
        this.selectedItem.id = undefined;
    }

    getDecryptedValue() {
        this.dataService.descrypt(this.selectedItem).pipe(
            tap(result => {
                this.isEncrypted = false;
                this.selectedItem = result;
            }, error => {
                console.log(error);
                this.openSnackBar('Error', error.statusText);
            }),
        ).subscribe();
    }

    openSnackBar(message: string, action: string) {
        this.snackBar.open(message, action, {
            duration: 2000
        });
    }

    private getData() {
        this.dataService.findAll().pipe(
            tap(success => {
                this.dataSource = success;
            }, error => {
                this.openSnackBar('Error', error.statusText);
            })
        ).subscribe();
    }

    ngOnDestroy(): void {
        this.dataSource = [];
        this.authenticationService.logout();
    }
}
