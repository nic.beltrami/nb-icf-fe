import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MatButtonModule, MatFormFieldModule, MatGridListModule, MatInputModule, MatTableModule, MatCardModule, MatSnackBarModule } from '@angular/material';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { DataListComponent } from './data-list/data-list.component';
import { HTTP_INTERCEPTORS, HttpClientModule, HttpClientXsrfModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { HttpXsrfInterceptor } from './services/http-xsr-interceptor';

@NgModule({
    declarations: [
        AppComponent,
        DataListComponent
    ],
    imports: [
        HttpClientModule,
        HttpClientXsrfModule.withOptions({
            cookieName: 'XSRF-TOKEN', // this is optional
            headerName: 'X-CSRF-TOKEN' // this is optional
        }),
        FormsModule,
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        NoopAnimationsModule,
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatTableModule,
        MatGridListModule,
        MatCardModule,
        MatSnackBarModule
    ],
    exports: [
        BrowserAnimationsModule,
        NoopAnimationsModule,
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatTableModule,
        MatGridListModule,
        MatCardModule,
        MatSnackBarModule
    ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: HttpXsrfInterceptor, multi: true }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
